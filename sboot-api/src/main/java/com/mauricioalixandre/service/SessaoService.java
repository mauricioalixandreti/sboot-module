package com.mauricioalixandre.service;

import java.time.LocalTime;
import java.util.List;

import org.springframework.stereotype.Service;

import com.mauricioalixandre.base.BaseService;
import com.mauricioalixandre.enums.StatusSessaoEnum;
import com.mauricioalixandre.exception.SistemaException;
import com.mauricioalixandre.model.Sessao;
import com.mauricioalixandre.repository.SessaoRepository;

@Service
public class SessaoService extends BaseService<Sessao, SessaoRepository> {

	public Sessao save(Sessao sessao) {
		try {
			if (sessao.getDuracao() != null 
					&& sessao.getDuracao().isBefore(LocalTime.parse("00:01"))){
				throw new SistemaException("Uma sessão deve durar no mínimo um minuto");
			}
			if(getRepository().existsByPautaId(sessao.getPauta().getId())) {
				throw new SistemaException("Já existe uma sessão cadastrada para esta pauta");
			}
			return salvarEntidade(sessao);
		}catch (Exception e) {
			throw e;
		}
	}
	
	public boolean isOpen(Long sessaoId) {
		return repository.existsByIdAndStatus(sessaoId, StatusSessaoEnum.ABERTA);
	}
	
	public List<Sessao> findSessoesEncerraveis(){
		return getRepository().findByStatusOrderByDuracaoAsc(StatusSessaoEnum.ABERTA);	
	}
	
}
