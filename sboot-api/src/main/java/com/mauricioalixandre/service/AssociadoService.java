package com.mauricioalixandre.service;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.mauricioalixandre.base.BaseService;
import com.mauricioalixandre.exception.SistemaException;
import com.mauricioalixandre.model.Associado;
import com.mauricioalixandre.repository.AssociadoRepository;

@Service
public class AssociadoService extends BaseService<Associado, AssociadoRepository> {
	

	public void update(Long id, Associado entity) {
		if(id == null || entity == null || entity.getId() == null) {
			throw new SistemaException("Dados invaálidos");			
		}
		if(id.equals(entity.getId())) {
			throw new SistemaException("O parametro id difere do id passadona entitdae");
		}
		Associado databaseEl = getRepository().findById(id).orElseGet(()->null);
		
		if(databaseEl == null) {
			throw new SistemaException("Entidade não encontrada");
		}
		BeanUtils.copyProperties(entity, databaseEl);
		getRepository().save(databaseEl);		
	}
	
	public Associado findByCpf(String cpf) {
		return getRepository().findByCpf(cpf);
	}
	
}
