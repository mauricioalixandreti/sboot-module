package com.mauricioalixandre.service;

import org.springframework.stereotype.Service;

import com.mauricioalixandre.base.BaseService;
import com.mauricioalixandre.model.Pauta;
import com.mauricioalixandre.repository.PautaRepository;

@Service
public class PautaService extends BaseService<Pauta, PautaRepository> {	
	

}
