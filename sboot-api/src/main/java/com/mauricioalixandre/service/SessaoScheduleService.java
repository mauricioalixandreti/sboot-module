package com.mauricioalixandre.service;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalAmount;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mauricioalixandre.config.ExceptionHandlerConfig;
import com.mauricioalixandre.enums.StatusSessaoEnum;
import com.mauricioalixandre.model.Sessao;
import com.mauricioalixandre.util.ApiUtil;

@Component
public class SessaoScheduleService {

	Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerConfig.class);

	private static final DateTimeFormatter outputFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
	
	@Autowired
	private SessaoService sessaoService;
	
	
	@Scheduled(fixedRate = 30000)
	@Transactional
	public void reportCurrentTime() {
		LocalDateTime lt = LocalDateTime.now(ZoneId.of("America/Sao_Paulo"));
		LOGGER.info("Inciando fechamento de sessão em {}", outputFormat.format(lt));
		sessaoService.findSessoesEncerraveis().forEach(sess->{
			verificcarEncerramento(sess);
		});;
	}
	
	private void verificcarEncerramento(Sessao sessao) {
		
		LocalTime current = LocalTime.now(ApiUtil.ZONA_ID_BR);
		
		LocalTime horaExcedente = sessao.getHoraInicio()
				.plusHours(sessao.getDuracao().getHour())
				.plusMinutes(sessao.getDuracao().getMinute())
				.plusSeconds(sessao.getDuracao().getSecond());
		
		boolean isHorarioExcedente = horaExcedente.isBefore(current); 
		
		if(isHorarioExcedente) {
			sessao.setStatus(StatusSessaoEnum.ENCERRADA);
			sessaoService.salvarEntidade(sessao);
			LOGGER.info("Realizando encerramento da Sessão: {}", sessao);
		}
	}
}
