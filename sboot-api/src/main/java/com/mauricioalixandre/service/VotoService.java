package com.mauricioalixandre.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mauricioalixandre.base.BaseService;
import com.mauricioalixandre.exception.SistemaException;
import com.mauricioalixandre.model.Associado;
import com.mauricioalixandre.model.Voto;
import com.mauricioalixandre.repository.VotoRepository;

@Service
public class VotoService extends BaseService<Voto, VotoRepository> {

	@Autowired
	private SessaoService sessaoService;
	
	@Autowired
	private AssociadoService associadoService;

	public Voto vote(Voto voto) {
		try {
			if (!sessaoService.isOpen(voto.getSessao().getId())) {
				throw new SistemaException("Não é permitido registrar voto para sessão encerrada");
			}
			Associado ass = associadoService.findByCpf(voto.getAssociado().getCpf());
			if(ass == null) {
				throw new SistemaException("Associado não cadastrado");
			}
			voto.setAssociado(ass);
			return  getRepository().save(voto);
		} catch (SistemaException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}

}
