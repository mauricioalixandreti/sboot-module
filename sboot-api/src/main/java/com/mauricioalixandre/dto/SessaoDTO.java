package com.mauricioalixandre.dto;

import java.time.LocalTime;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mauricioalixandre.model.EntityConveter;
import com.mauricioalixandre.model.Pauta;
import com.mauricioalixandre.model.Sessao;
import com.mauricioalixandre.serializer.LocalTimeDeserializer;
import com.mauricioalixandre.serializer.LocalTimeSerializer;

public class SessaoDTO extends BaseDTO implements EntityConveter<Sessao> {

	@JsonIgnore
	private static final long serialVersionUID = 1134428225351795980L;

	@NotNull(message = "Informe co codigo da pauta")
	private Long pautaId;

	@NotNull(message = "Informe a duração da sessão")
	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonDeserialize(using = LocalTimeDeserializer.class)
	private LocalTime duracao;

	@Override
	@JsonIgnore
	public Sessao toEntity() {
		Sessao sessao = new Sessao();
		sessao.setDuracao(duracao);
		sessao.setPauta(new Pauta(pautaId));
		return sessao;
	}

	public Long getPautaId() {
		return pautaId;
	}

	public void setPautaId(Long pautaId) {
		this.pautaId = pautaId;
	}

	public LocalTime getDuracao() {
		return duracao;
	}

	public void setDuracao(LocalTime duracao) {
		this.duracao = duracao;
	}

}
