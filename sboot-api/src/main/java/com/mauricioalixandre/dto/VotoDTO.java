package com.mauricioalixandre.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;
import org.springframework.validation.annotation.Validated;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mauricioalixandre.annotation.CpfConstaint;
import com.mauricioalixandre.enums.VotoEnum;
import com.mauricioalixandre.model.Associado;
import com.mauricioalixandre.model.EntityConveter;
import com.mauricioalixandre.model.Sessao;
import com.mauricioalixandre.model.Voto;

@Validated
public class VotoDTO extends BaseDTO implements EntityConveter<Voto> {
	
	@JsonIgnore
	private static final long serialVersionUID = 4134428225351795980L;
	
	@NotNull(message = "Informe a sessão de votação")
	private Long sessaoId;
	
	@NotEmpty(message = "Informe o associado")
	@CPF(message = "CPF com formato inválido")
	@CpfConstaint(message = "CPF sem permissão para votar")
	private String cpfAssociado;	
	
	@NotNull(message = "Informe o voto do associado")
	private VotoEnum voto;
	
	public Long getSessaoId() {
		return sessaoId;
	}
	public void setSessaoId(Long sessaoId) {
		this.sessaoId = sessaoId;
	} 
 
	public String getCpfAssociado() {
		return cpfAssociado;
	}
	public void setCpfAssociado(String cpfAssociado) {
		this.cpfAssociado = cpfAssociado;
	}
	public VotoEnum getVoto() {
		return voto;
	}
	public void setVoto(VotoEnum voto) {
		this.voto = voto;
	}
	
	@Override
	@JsonIgnore
	public Voto toEntity() {
		Voto vt = new Voto();
		vt.setId(getId());
		vt.setSessao(new Sessao(getSessaoId()));
		vt.setAssociado(new Associado(getCpfAssociado()));
		vt.setVoto(getVoto());
		return vt;		
	}
	
	
}
