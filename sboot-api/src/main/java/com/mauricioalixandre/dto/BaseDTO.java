package com.mauricioalixandre.dto;

public class BaseDTO {
	
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
