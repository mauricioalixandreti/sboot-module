package com.mauricioalixandre.repository;

import java.util.List;

import com.google.common.primitives.Longs;
import com.mauricioalixandre.base.BaseRepository;
import com.mauricioalixandre.enums.StatusSessaoEnum;
import com.mauricioalixandre.model.Sessao;

public interface SessaoRepository extends BaseRepository<Sessao> {

	Sessao findByPautaId(Longs pautaId);

	boolean existsByPautaId(Long pautaId);
	
	boolean existsByIdAndStatus(Long sessaoId, StatusSessaoEnum status);

	List<Sessao> findByStatusOrderByDuracaoAsc(StatusSessaoEnum status);

}
