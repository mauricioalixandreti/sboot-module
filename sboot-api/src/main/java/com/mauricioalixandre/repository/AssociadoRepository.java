package com.mauricioalixandre.repository;

import com.mauricioalixandre.base.BaseRepository;
import com.mauricioalixandre.model.Associado;

public interface AssociadoRepository extends BaseRepository<Associado>{

	Associado findByCpf(String cpf);
	
}
