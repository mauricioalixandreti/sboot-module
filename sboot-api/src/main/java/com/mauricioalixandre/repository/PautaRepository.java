package com.mauricioalixandre.repository;

import com.mauricioalixandre.base.BaseRepository;
import com.mauricioalixandre.model.Pauta;

public interface PautaRepository extends BaseRepository<Pauta>{

}
