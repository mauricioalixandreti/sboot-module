package com.mauricioalixandre.repository;

import com.mauricioalixandre.base.BaseRepository;
import com.mauricioalixandre.model.Associado;
import com.mauricioalixandre.model.Voto;

public interface VotoRepository extends BaseRepository<Voto>{

}
