package com.mauricioalixandre.annotation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.mauricioalixandre.annotation.Unique.List;

@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE, TYPE })
@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = {UniqueConstraintValidator.class})
@Repeatable(List.class)
public @interface Unique {
	
	String message() default "Campo deve ter valor único";
	
	String propertyPath(); 
    
	Class<?>[] groups() default { };
    
    Class<? extends Payload>[] payload() default { };
    
    @Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE, TYPE })
	@Retention(RUNTIME)
    @Documented
    @interface List {
    	Unique[] value();
    }
}
