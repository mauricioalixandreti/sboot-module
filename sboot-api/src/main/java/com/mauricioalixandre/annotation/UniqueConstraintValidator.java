package com.mauricioalixandre.annotation;

import java.lang.reflect.Field;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class UniqueConstraintValidator implements ConstraintValidator<Unique,Object> {
	
	Logger LOGGER = LogManager.getLogger(UniqueConstraintValidator.class);	
	
	@PersistenceContext
	private EntityManager entityManager;
	private String message;
	private String propertyPath;
	private static final String QUERY_FIND = "SELECT o from %s o where %s = '%s' ";
	
	@Override
    public void initialize(Unique unique) {
		message = unique.message();
		propertyPath = unique.propertyPath();
    }
	
	@Override
	public boolean isValid(Object entity, ConstraintValidatorContext context) {
		message.toString();
		try {
			Field field = entity.getClass().getDeclaredField(propertyPath);
			if(field == null) return true;
			field.setAccessible(true);
			Object fieldValue = field.get(entity);
			if(fieldValue == null) return true;
			String sql = String.format(QUERY_FIND, entity.getClass().getSimpleName(), field.getName(), fieldValue.toString());
			Object  result = entityManager.createQuery(sql, entity.getClass()).getSingleResult();
			return result == null;
		} catch (NoResultException e) {			
			return true;
		} catch (Exception e) {
			LOGGER.debug("Erro ao executar UniqueContrait validations", e);
			return false;
		} 
	}

}
