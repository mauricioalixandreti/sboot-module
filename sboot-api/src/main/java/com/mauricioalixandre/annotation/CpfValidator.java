package com.mauricioalixandre.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.mauricioalixandre.integration.CpfValidatorService;


public class CpfValidator implements ConstraintValidator<CpfConstaint, String> {

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		CpfValidatorService cpfIntegrationService = new CpfValidatorService();
		return cpfIntegrationService.validarCpf(value);
	}

}
