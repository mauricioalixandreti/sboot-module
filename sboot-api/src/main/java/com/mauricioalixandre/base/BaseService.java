package com.mauricioalixandre.base;


import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import javax.transaction.Transactional.TxType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mauricioalixandre.exception.SistemaException;
import com.mauricioalixandre.model.Associado;

public class BaseService<T extends BaseEntity, K extends BaseRepository<T>> {

	protected static final Logger LOGGER = LogManager.getLogger(BaseService.class.getName());
	
	@Autowired
	protected K repository;  
	
	@Autowired
	protected EntityManager em; 
	
	@Autowired
	protected static ApplicationContext applicationContext;
	
	@Transactional(rollbackOn = Throwable.class)
	public T salvarEntidade(T entidade) {
		return repository.save(entidade);
	}


	@Transactional
	public void deleteById(Long id) {
		this.repository.deleteById(id);;
	}

	public List<T> listAll() {
		return repository.findAll();
	}
	
	public Page<T> listAllPaginado(Pageable pageable){
		return repository.findAll(pageable);
	}

	public Page<T> listAllPaginadoComFiltro(Example<T> filtro, Pageable pageable){
		return repository.findAll(filtro, pageable);
	}
	
	public void update(Long id, T entity) {
		if(id == null || entity == null || entity.getId() == null) {
			throw new SistemaException("Dados invaálidos");			
		}
		if(id.equals(entity.getId())) {
			throw new SistemaException("O parametro id difere do id informado nal entitdae");
		}
		T databaseEl = getRepository().findById(id).orElseGet(()->null);
		
		if(databaseEl == null) {
			throw new SistemaException("Entidade não encontrada");			
		}
		BeanUtils.copyProperties(entity, databaseEl);;
		repository.save(databaseEl)	;	
	}
	
	public K getRepository(){
		return repository;
	} 

	public void setRepository(K repository) {
		this.repository = repository;
	}

	public EntityManager getEm() {
		return em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}
 

}

