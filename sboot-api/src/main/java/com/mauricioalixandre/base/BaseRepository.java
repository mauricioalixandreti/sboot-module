package com.mauricioalixandre.base; 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface BaseRepository<T extends Object> extends JpaRepository<T, Object>, CrudRepository<T, Object>, JpaSpecificationExecutor<T> {
   
	
}
