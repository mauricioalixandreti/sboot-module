package com.mauricioalixandre.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class BaseResource<U extends BaseService<? extends BaseEntity, ? extends BaseRepository<?>>> {

	@Autowired
	protected U service;

	public U getService() {
		return service;
	}

	public ResponseEntity<?> ok(Object body) {
		return new ResponseEntity<>(body, HttpStatus.OK);
	}

	public ResponseEntity<?> notFound(Object body) {
		return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<?> created(Object body) {
		return new ResponseEntity<>(body, HttpStatus.CREATED);
	}

	public ResponseEntity<?> internalServerError(Object body) {
		return new ResponseEntity<>(body, HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
