package com.mauricioalixandre.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.mauricioalixandre.base.BaseEntity;

public class BaseEntityIdSerializer extends JsonSerializer<BaseEntity> {

	@Override
	public void serialize(BaseEntity value, JsonGenerator gen, SerializerProvider serializers)
			throws IOException, JsonProcessingException { 
		gen.writeString(value.getId().toString());		
	}

	 

}
