package com.mauricioalixandre.serializer;

import java.io.IOException;
import java.time.LocalTime;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.mauricioalixandre.exception.SistemaException;

public class LocalTimeDeserializer extends JsonDeserializer<LocalTime> {
	
	private static final String MSG = "Erro ao serializar hora hora  %s";
	
	@Override
	public LocalTime deserialize(JsonParser p, DeserializationContext ctxt)
			throws IOException, JsonProcessingException {
		try {
			String valorHora = p.getText();
			return LocalTime.parse(valorHora);
		}catch (Exception e) {
			throw new SistemaException(String.format(MSG, p.getText()));
		}
	}

}
