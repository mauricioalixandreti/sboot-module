package com.mauricioalixandre.config;

import java.util.HashMap;
import java.util.Map;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mauricioalixandre.exception.SistemaException;


@ControllerAdvice
public class ExceptionHandlerConfig {
	
	
	Logger LOGGER = LoggerFactory.getLogger(ExceptionHandlerConfig.class);

	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	@ExceptionHandler({ MethodArgumentNotValidException.class })
	@ResponseBody
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
		Map<String, String> errors = new HashMap<>();
		int errorCount = 1;
		LOGGER.debug("error",ex);
		ex.getBindingResult().getAllErrors().forEach((error) -> {
//			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put("infoN"+errorCount, errorMessage);
		});
		return errors;
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(SistemaException.class)
	@ResponseBody
	public Map<String, String> erroIntegracao(SistemaException ex) {
		Map<String, String> errors = new HashMap<>();
		LOGGER.debug("error",ex);
		errors.put("msg", ex.getMessage());
		return errors;
	}

	@ResponseStatus(HttpStatus.CONFLICT)
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseBody
	public Map<String, String> constraint(ConstraintViolationException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.printStackTrace();
		LOGGER.debug("error",ex);
		return errors;
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler({ javax.validation.ConstraintViolationException.class })
	@ResponseBody
	public Map<String, String> uniqueConstraint(javax.validation.ConstraintViolationException ex) {
		Map<String, String> errors = new HashMap<>();
		ex.getConstraintViolations().forEach(ctv->{
			String fledName = ctv.getPropertyPath().iterator().next().getName();  
			String messageString = ctv.getMessage();
			errors.put(fledName, messageString);			    
		});
		LOGGER.debug("error",ex);	
//		errors.put("cpf", "duplicado javax.validation.ConstraintViolationException");
		return errors;
	}

}
