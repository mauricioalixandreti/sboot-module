package com.mauricioalixandre.model;

import com.mauricioalixandre.base.BaseEntity;

public interface EntityConveter<E extends BaseEntity> {

	E toEntity(); 
	
}
