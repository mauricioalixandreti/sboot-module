package com.mauricioalixandre.model;

import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mauricioalixandre.base.BaseEntity;
import com.mauricioalixandre.enums.StatusSessaoEnum;
import com.mauricioalixandre.serializer.BaseEntityIdSerializer;
import com.mauricioalixandre.serializer.LocalTimeSerializer;
import com.mauricioalixandre.util.ApiUtil;
import com.sun.istack.NotNull;

@Entity
public class Sessao extends BaseEntity{
 
	private static final long serialVersionUID = 45551L;

	@OneToOne
	@JoinColumn(name = "pauta_id")
	@NotNull
	@JsonSerialize(using = BaseEntityIdSerializer.class)
	private Pauta pauta;
	
	@Column
	@NotNull
	@JsonSerialize(using = LocalTimeSerializer.class)
	private LocalTime duracao;
	
	@Column
	@JsonSerialize(using = LocalTimeSerializer.class)
	private LocalTime horaInicio;
	
	@Column
	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonIgnore
	private LocalDate dataSessao;
	
	@OneToMany(mappedBy = "sessao", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Voto> votos;

	@Column
	@Enumerated(EnumType.STRING)
	@JsonIgnore
	private StatusSessaoEnum status;
	
	public Sessao() {
		super();
	}
	
	public Sessao(Long id) {
		super();
		setId(id);
	}

	@PrePersist
	private void preSave() {
		setDataSessao(LocalDate.now(ApiUtil.ZONA_ID_BR));
		setHoraInicio(LocalTime.now(ApiUtil.ZONA_ID_BR));		
		setStatus(StatusSessaoEnum.ABERTA);
		if(duracao == null) {
			setDuracao(LocalTime.of(0, 1));
		}
	} 
	

	public Pauta getPauta() {
		return pauta;
	}

	public void setPauta(Pauta pauta) {
		this.pauta = pauta;
	}

	public LocalTime getDuracao() {
		return duracao;
	}

	public void setDuracao(LocalTime duracao) {
		this.duracao = duracao;
	}

	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	public LocalDate getDataSessao() {
		return dataSessao;
	}

	public void setDataSessao(LocalDate dataSessao) {
		this.dataSessao = dataSessao;
	}


	public List<Voto> getVotos() {
		return votos;
	}


	public void setVotos(List<Voto> votos) {
		this.votos = votos;
	}


	public StatusSessaoEnum getStatus() {
		return status;
	}


	public void setStatus(StatusSessaoEnum status) {
		this.status = status;
	}
 
	
	
	
	
}
