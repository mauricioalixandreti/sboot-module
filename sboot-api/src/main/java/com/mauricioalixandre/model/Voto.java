package com.mauricioalixandre.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mauricioalixandre.base.BaseEntity;
import com.mauricioalixandre.enums.VotoEnum;
import com.mauricioalixandre.serializer.BaseEntityIdSerializer;

@Entity
public class Voto extends BaseEntity { 
 
	private static final long serialVersionUID = 1L;

	@ManyToOne
	@JoinColumn(name = "sessao_id")
	@JsonSerialize(using = BaseEntityIdSerializer.class)
	private Sessao sessao;	
	
	@ManyToOne
	@JoinColumn(name = "associado_id")
	@JsonSerialize(using = BaseEntityIdSerializer.class)
	private Associado associado;
	
	@Column
	@Enumerated(EnumType.STRING)
	private VotoEnum voto;
	
	public Voto() {
		super();
	}
	
	public Voto(Long id) {
		super();
		setId(id);
	}

	public Sessao getSessao() {
		return sessao;
	}

	public void setSessao(Sessao sessao) {
		this.sessao = sessao;
	}

	public Associado getAssociado() {
		return associado;
	}

	public void setAssociado(Associado associado) {
		this.associado = associado;
	}

	public VotoEnum getVoto() {
		return voto;
	}

	public void setVoto(VotoEnum voto) {
		this.voto = voto;
	}
	
	
	
	
}
