package com.mauricioalixandre.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CPF;

import com.mauricioalixandre.annotation.CpfConstaint;
import com.mauricioalixandre.annotation.Unique;
import com.mauricioalixandre.base.BaseEntity;

@Entity
@Unique.List({ 
	@Unique(propertyPath = "cpf", message = "CPF não pode ser duplicado"),
	@Unique(propertyPath = "email", message = "E-mail não pode ser duplicado") 
})
public class Associado extends BaseEntity {

	private static final long serialVersionUID = 4134428215351795980L;
	@Column
	@Email(message = "E-mail inválio")
	private String email;
	@Column
	@NotNull
	@CPF(message = "CPF com formato inválido")
	@CpfConstaint(message = "CPF sem permissão para votar")
	private String cpf;
	@Column
	@NotNull
	private String primeiroNome;
	@Column
	private String ultimoNome;
	
	public Associado() {
		super();
	}
	
	public Associado(Long id) {
		super();
		setId(id);
	} 
	
	public Associado(String cpf) {
		super();
		setCpf(cpf);
	} 

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getPrimeiroNome() {
		return primeiroNome;
	}

	public void setPrimeiroNome(String primeiroNome) {
		this.primeiroNome = primeiroNome;
	}

	public String getUltimoNome() {
		return ultimoNome;
	}

	public void setUltimoNome(String ultimoNome) {
		this.ultimoNome = ultimoNome;
	}

}
