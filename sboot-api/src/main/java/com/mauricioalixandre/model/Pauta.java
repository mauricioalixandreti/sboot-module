package com.mauricioalixandre.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrePersist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mauricioalixandre.base.BaseEntity;
import com.mauricioalixandre.serializer.LocalTimeSerializer;
import com.sun.istack.NotNull;

@Entity
public class Pauta extends BaseEntity {
 
	private static final long serialVersionUID = 3747823728584269774L;
	
	@Column
	@NotNull
	private String tema;
	
	@Column	
	@NotNull
	private String descricao;
	
	@Column	
	@JsonSerialize(using = LocalTimeSerializer.class)
	@JsonIgnore
	private LocalDate dtCriacao;
	
	public Pauta() {
		super();
	}
	
	public Pauta(Long id) {
		super();
		setId(id);
	}
	
	@PrePersist
	private void preSave() {
		setDtCriacao(LocalDate.now());
	}
	
	public String getTema() {
		return tema;
	}
	public void setTema(String tema) {
		this.tema = tema;
	} 
	public LocalDate getDtCriacao() {
		return dtCriacao;
	}
	public void setDtCriacao(LocalDate dtCriacao) {
		this.dtCriacao = dtCriacao;
	}
	
	
}
