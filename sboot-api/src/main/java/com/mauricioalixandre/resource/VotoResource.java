package com.mauricioalixandre.resource;


import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mauricioalixandre.base.BaseResource;
import com.mauricioalixandre.dto.VotoDTO;
import com.mauricioalixandre.service.VotoService;

@RestController
@RequestMapping("votos")
public class VotoResource extends BaseResource<VotoService> {
	
	@GetMapping
	public ResponseEntity<?> get(Pageable page){
		return ResponseEntity.ok(service.listAllPaginado(page));
	}	
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<?> save(@RequestBody @Valid VotoDTO associado){
		return created(service.vote(associado.toEntity()));
	}
	
	

}
