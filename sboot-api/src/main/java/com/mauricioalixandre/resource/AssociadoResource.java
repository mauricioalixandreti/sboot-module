package com.mauricioalixandre.resource;


import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mauricioalixandre.base.BaseResource;
import com.mauricioalixandre.model.Associado;
import com.mauricioalixandre.service.AssociadoService;

@RestController
@RequestMapping("associados")
//@Validated
public class AssociadoResource extends BaseResource<AssociadoService> {
	
	@GetMapping
	@ResponseStatus(code = HttpStatus.OK)
	public ResponseEntity<?> get(Pageable page){
		return ResponseEntity.ok(service.listAllPaginado(page));
	}
	
	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public ResponseEntity<?> save(@RequestBody @Valid Associado associado){
		return created(service.salvarEntidade(associado));
	}
	
	@DeleteMapping("{id}")	
	@ResponseStatus(code = HttpStatus.OK)
	public void delete(@PathVariable Long id){
		service.deleteById(id);
	}
	
	@PutMapping("{id}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	public void update(@PathVariable Long id, @RequestBody Associado entity){
		service.update(id, entity);
	}

}
