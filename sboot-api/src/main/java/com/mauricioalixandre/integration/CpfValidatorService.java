package com.mauricioalixandre.integration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.mauricioalixandre.exception.SistemaException;

@Service
public class CpfValidatorService {
	
	Logger LOGGER = LogManager.getLogger(CpfValidatorService.class);	
	private RestTemplate client;
	private static final String URL_SERVICE = "https://user-info.herokuapp.com/users/{cpf}";
	
	public CpfValidatorService() {
		client =  new RestTemplate();
	}
	
	public boolean validarCpf(String cpf) {
		try {			
			CpfIntegrationDTO result = client.getForObject(URL_SERVICE, CpfIntegrationDTO.class, cpf);
			return result.getStatus().equals("ABLE_TO_VOTE");
		}catch (Exception e) {
			LOGGER.debug("Erro de integração ao validar CPF", e);
			throw new SistemaException("Erro de integração ao validar CPF");
		}
	}

}
