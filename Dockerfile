FROM java:8-alpine

RUN apk add --update ca-certificates && rm -rf /var/cache/apk/* && \
  find /usr/share/ca-certificates/mozilla/ -name "*.crt" -exec keytool -import -trustcacerts \
  -keystore /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/cacerts -storepass changeit -noprompt \
  -file {} -alias {} \; && \
  keytool -list -keystore /usr/lib/jvm/java-1.8-openjdk/jre/lib/security/cacerts --storepass changeit

ENV MAVEN_VERSION 3.5.4
ENV MAVEN_HOME /usr/lib/mvn
ENV PATH $MAVEN_HOME/bin:$PATH

RUN wget http://archive.apache.org/dist/maven/maven-3/$MAVEN_VERSION/binaries/apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  tar -zxvf apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  rm apache-maven-$MAVEN_VERSION-bin.tar.gz && \
  mv apache-maven-$MAVEN_VERSION /usr/lib/mvn

RUN mkdir -p /home/parent

COPY ./sboot-parent/ /home/parent/sboot-parent
COPY ./sboot-api/ /home/parent/sboot-api
COPY ./pom.xml /home/parent/pom.xml

RUN ls -l /home/parent

RUN mvn -f /home/parent/sboot-api/pom.xml clean package 

RUN ls -l /home/parent/sboot-api

WORKDIR /home/parent

EXPOSE 8080

ENTRYPOINT ["java","-jar","-Dspring.profiles.active=dev","/home/parent/sboot-api/target/sboot-api.jar"]

#RUN mkdir -p /usr/src/app
#WORKDIR /usr/src/app